import getopt
import sys

from cassandra.auth import PlainTextAuthProvider
from cassandra.cluster import Cluster
from cassandra.query import dict_factory
import os

password = "cassandra"
from_cluster_ip = ""
to_keyspace="no_keyspace"
argumentList = sys.argv[1:]
options = "p:c:k:"

try:
    arguments, values = getopt.getopt(argumentList, options)
    for currentArgument, currentValue in arguments:
        if currentArgument in ("-p"):
            password = currentValue
        elif currentArgument in ("-c"):
            from_cluster_ip = currentValue
        elif currentArgument in ("-k"):
            to_keyspace = currentValue
except getopt.error as err:
    print(str(err))

auth_provider = PlainTextAuthProvider(
    username='cassandra', password='cassandra')
cluster = Cluster([from_cluster_ip], auth_provider=auth_provider, control_connection_timeout=1000, port=9042)
cassandra_session = cluster.connect('quicko')

completed_tables = []

os.system("touch completedtables.txt")
with open("completedtables.txt") as file1:
    completed_tables=file1.readlines()

completed_tables=[x.replace("\n","") for x in completed_tables ]
print("Skipping already migrated tables :")
print(completed_tables)
tables = cassandra_session.execute("SELECT * FROM system_schema.tables WHERE keyspace_name = 'quicko'")
for row in tables:
    if "_by_" not in row.table_name and row.table_name not in completed_tables and "content_history" not in row.table_name and "flows_deleted" not in row.table_name:
        print(row.table_name)
        os.system("mkdir " + row.table_name)
        cqlsh_command = "cqlsh 78.46.214.26 --connect-timeout 45 -u cassandra -p cassandra -e \"COPY quicko." + row.table_name + " TO '/home/rajat.hegde/" + row.table_name + "/" + row.table_name + ".csv' WITH HEADER=true AND PAGETIMEOUT=40 AND MAXOUTPUTSIZE=1000000 AND DELIMITER='|';\""
        os.system(cqlsh_command)
        directory = os.fsencode("/home/rajat.hegde/" + row.table_name)
        for file in os.listdir(directory):
            filename = os.fsdecode(file)
            print(filename)

            cqlsh_copyfrom = "cqlsh 65.108.92.39 --connect-timeout 45 -e \"COPY "+to_keyspace +"." + row.table_name + " FROM '/home/rajat.hegde/" + row.table_name + "/" + filename + "' WITH DELIMITER='|' AND HEADER=TRUE AND CHUNKSIZE=100 AND MAXBATCHSIZE=5\""
            print(cqlsh_copyfrom)
            os.system(cqlsh_copyfrom)
        os.system("rm -rf " + row.table_name)
        with open("completedtables.txt", "a+") as file1:
            file1.write(row.table_name+"\n")
print("Table migration complted!")