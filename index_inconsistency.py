import getopt
import json
import sys
import time
from datetime import datetime
from typing import Dict

from cassandra.cluster import Cluster, Session
from cassandra.query import dict_factory
import logging

# set logger config
logging.basicConfig(filename='index_correction_' + datetime.now().strftime("%b-%d-%Y-%H-%M-%S"),
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
query_file = open("duplicate_check_queries_" + datetime.now().strftime("%b-%d-%Y-%H-%M-%S"), "a")
""" 
Global data block

"""

# cassandra ip to connect passed through argument
cassandra_ip: str

# Path of wgestor to read meta data
wgestor_path: str

""" 
Base table name on which you want to run the script
It accepts 
1. table_name
2. all (Fixes all table)
"""
table: str

# Indexer range on which the script runs
indexer_range: str

# Connect to cassandra and get session
session: Session

# It says if the script runs in read or write mode
read_mode: bool = False

"""
It says about script execution mode
1 -> Fix duplicate entries
2 -> Fix orphan entries
3 -> Fix missing entries
"""
operation: str

"""
Gives table to primary key mapping
flows->flow_id
"""
table_to_primary_key_mapping: Dict[str, str] = {}

"""
Gives table to clustering key mapping
tags->tag_id
"""
table_to_clustering_key_mapping: Dict[str, str] = {}

"""Gives table to unprocessed index mapping
flows->runs,published_at,type,tags:runs
"""
table_to_raw_index_mappings: dict = {}

"""Gives table to index tables mapping 
flows -> flows_by_runs,flows_by_published_at,....
"""
table_to_index_table_mapping = {}

"""Gives table to index tables mapping 
flows -> ent_id
"""
table_to_indexer_mapping = {}

"""Gives table to index tables mapping 
flows_by_tags_published_at -> ['tags', 'published_at']
"""
table_to_index_fields_mapping = {}

# Gives details about tables which are of type transposable
transposable_tables = set()

# Stores raw query -> prepared query mapping
prepared_statement_cache = {}

"""
End of global data block
"""

"""
To get run time details related to script we parse arguments
"""

argumentList = sys.argv[1:]
options = "hw:o:t:i:r:n:"
long_options = ["help", "wgestorlocation =", "operation =", "table =", "indexer =", "read_mode =", "cass_ip ="]

try:
    arguments, values = getopt.getopt(argumentList, options, long_options)
    for currentArgument, currentValue in arguments:

        if currentArgument in ("-h", "--Help"):
            help = """
            -w or --wgestorlocation - Provide wgestor file location
            -ip or --cass_ip - provide cassandra ip
            -t or --table - provide table value: table name or 'all'
            -i or --indexer - provide range indexer value: 'all' or 'one' or 'selected'
            -r or --read_mode - provide 'Y' if script should run in read mode 
            -o or --option - provide script type: 1 for fix duplicates 2 for fix orphans 3 for fix missing entries
            """
            print(help)
            exit()
        elif currentArgument in ("-w", "--wgestorlocation"):
            wgestor_path = currentValue
        elif currentArgument in ("-n", "--cass_ip"):
            cassandra_ip = currentValue
        elif currentArgument in ("-t", "--table"):
            table = currentValue
        elif currentArgument in ("-i", "--indexer"):
            indexer_range = currentValue
        elif currentArgument in ("-r", "--read_mode"):
            read_mode = currentValue == "Y" or currentValue == "y"
        elif currentArgument in ("-o", "--operation"):
            operation = currentValue
except getopt.error as err:
    print(str(err))

"""
Connect to cassandra and get session
"""


def __cassandra_connect():
    cluster = Cluster([cassandra_ip], control_connection_timeout=1000, port=9042)
    cassandra_session = cluster.connect('quicko')
    cassandra_session.row_factory = dict_factory
    return cassandra_session


session = __cassandra_connect()

"""
Block to extract meta data from wgestor
"""


def __read_from_wgestor():
    # wgestor_data is a dictionary formed by parsing wgestor.json file
    wgestor_data = json.load(open(wgestor_path, ))
    for gestor in wgestor_data['gestors']:
        table_name: str
        if 'cassandra_column_family' in gestor['environment'].keys():
            table_name = gestor['environment']['cassandra_column_family']
            # populate indexer values
            if 'indexer' in gestor['environment'].keys():
                table_to_indexer_mapping[table_name] = gestor['environment'][
                    'indexer']
            # populate primary and clustering key map
            if 'options' in gestor.keys():
                for option in gestor['options']:
                    if option['type'] == 'key':
                        table_to_primary_key_mapping[table_name] = option['name']
                    if option['type'] == 'clustering_column' or option['type'] == 'column_dynamic_map' or option[
                        'type'] == 'column_dynamic':
                        table_to_clustering_key_mapping[table_name] = option[
                            'name']
        # Get index values
        if 'index' in gestor['environment'].keys():
            # there are multiple occurances of table in wgestor file e.g : flows,videos,links all have same base table
            # we are getting longest index from it so that we don't miss any index table
            raw_index_value: str = gestor['environment']['index']
            if table_name in table_to_raw_index_mappings.keys():
                if len(table_to_raw_index_mappings[table_name]) < len(
                        raw_index_value):
                    table_to_raw_index_mappings[table_name] = raw_index_value
            else:
                table_to_raw_index_mappings[table_name] = raw_index_value
        if gestor['clz'] == 'co.quicko.api.gestor.cassandra.TransposableCassandraGestor':
            transposable_tables.add(table_name)


# set index values from raw index values in wgestor
def __set_index_table_details_in_global_data():
    for base_table in table_to_raw_index_mappings.keys():
        table_to_index_table_mapping[base_table] = []
        for index_table_name in table_to_raw_index_mappings[base_table].split(","):
            index_table_builder = '{}_by'.format(base_table)
            for sub_index_name in index_table_name.split(":"):
                index_table_builder = "{}_{}".format(index_table_builder, sub_index_name)
            table_to_index_table_mapping[base_table].append(index_table_builder)

            # to add index fields
            table_to_index_fields_mapping[index_table_builder] = []
            for sub_index_name in index_table_name.split(":"):
                table_to_index_fields_mapping[index_table_builder].append(sub_index_name)


"""
Invoke read_from_wgestor and __set_index_table_details_in_global_data functions to set global data
"""

__read_from_wgestor()
__set_index_table_details_in_global_data()
logger.debug(table_to_clustering_key_mapping)
logger.debug(table_to_raw_index_mappings)
logger.debug(table_to_indexer_mapping)
logger.debug(table_to_primary_key_mapping)
logger.debug(table_to_index_fields_mapping)
logger.debug(table_to_index_table_mapping)

"""
Query builder block:  get query from it's parameters (select,where,table)
"""


def get_query_for_all_select_elements_from_table(where_elements, table_name):
    return get_query_from_paramters('*', where_elements, table_name)


def get_query_for_selected_elements_from_table(select_elements, where_elements, table_name):
    select = ','.join(select_elements)
    return get_query_from_paramters(select, where_elements, table_name)


def get_query_from_paramters(select, where_elements, table_name):
    actual_where_list = [element + '=?' for element in where_elements]
    where = ' and '.join(actual_where_list)
    query = 'select {} from {} where {}'.format(select, table_name, where)
    return query


def get_actual_executed_query(row, table_name):
    actual_where_list = []
    for key in row.keys():
        if not key == 'timestamp':
            actual_where_list.append('{} = \'{}\''.format(key, row[key]))
    where = ' and '.join(actual_where_list)
    select = ','.join(row.keys())
    query = 'select {} from {} where {}'.format(select, table_name, where)
    return query


# Get prepared statement from cache if present
def __get_prepared_statement(query):
    if query in prepared_statement_cache.keys():
        return prepared_statement_cache[query]
    else:
        prepared_statement_cache[query] = session.prepare(query)
        return prepared_statement_cache[query]


"""
Fixes duplicate entries in index tables
"""


def __fix_duplicate_entries(indexer, base_table):
    if not base_table in table_to_index_table_mapping.keys():
        logger.warning('No index table for this base table : ' + base_table)
        return
    for index_table in table_to_index_table_mapping[base_table]:
        logger.warning("Fixing table " + index_table)
        if not base_table in table_to_indexer_mapping.keys():
            logger.warning("No indexer for this table " + index_table)
            table_to_indexer_mapping[base_table] = 'key'
        query = get_query_for_all_select_elements_from_table([table_to_indexer_mapping[base_table]],
                                                             index_table)
        prepared_query = __get_prepared_statement(query)
        index_table_result = session.execute(prepared_query, (indexer,))
        __detect_and_delete_duplicate_entries(index_table_result,
                                              base_table,
                                              index_table)
        logger.warning("Fixed table " + index_table)


# Fix duplicate entries
def __detect_and_delete_duplicate_entries(index_table_content, base_table, index_table):
    primary_key = table_to_primary_key_mapping[base_table]
    primary_key_set = set()
    if base_table in table_to_clustering_key_mapping.keys():
        clustering_column = table_to_clustering_key_mapping[base_table]
        for row in index_table_content:
            if (row[primary_key], row[clustering_column]) in primary_key_set:
                __get_all_duplicates_and_delete_from_index_table(index_table, row)
            else:
                primary_key_set.add((row[primary_key], row[clustering_column]))
    else:
        for row in index_table_content:
            if row[primary_key] in primary_key_set:
                __get_all_duplicates_and_delete_from_index_table(index_table, row)
            else:
                primary_key_set.add(row[primary_key])


# Delete Query
def __delete_row(row, table):
    where_list = [element + '=?' for element in row.keys()]
    where = ' and '.join(where_list)
    delete_query = 'DELETE FROM {} where {}'.format(table, where)
    logger.warning("Executing Delete on row " + str(row))
    prepared_delete_query = __get_prepared_statement(delete_query)
    session.execute(prepared_delete_query, tuple(row.values()))
    logger.warning("Executed delete on row " + str(row))


# Delete from Index table
def __get_all_duplicates_and_delete_from_index_table(table, row):
    # pops timestamp to get all duplicate rows
    row.pop('timestamp', None)
    # Query to get all duplicate rows
    select_query = get_query_for_all_select_elements_from_table(row.keys(), table)
    prepared_select_query = __get_prepared_statement(select_query)
    duplicate_rows = session.execute(prepared_select_query, tuple(row.values()))
    timestamp_to_delete = []
    for row_result in duplicate_rows:
        timestamp_to_delete.append(row_result['timestamp'])
    timestamp_to_delete = sorted(timestamp_to_delete)
    for i in range(len(timestamp_to_delete) - 1):
        row['timestamp'] = timestamp_to_delete[i]
        logger.warning("Found a duplicate row with same primary key " + str(row))
        if read_mode:
            logger.warning(get_actual_executed_query(row, table))
        query_file.write('\n' + get_actual_executed_query(row, table))
        if not read_mode:
            __delete_row(row, table)


"""
End of fix duplicate entries block
"""

"""
Start of fix orphan entries block
"""


def __fix_orphan_entries(indexer, base_table):
    # set to keep track of already verified entries
    if not base_table in table_to_index_table_mapping.keys():
        logger.warning('No index table for this base table : ' + base_table)
        return
    orphan_primary_key_set = set()
    non_orphan_primary_key_set = set()
    for individual_index_table in table_to_index_table_mapping[base_table]:
        logger.warning("Fixing table " + individual_index_table)
        query = 'SELECT * FROM {} where {} =?'.format(individual_index_table, table_to_indexer_mapping[base_table])
        prepared_query = __get_prepared_statement(query)
        result = session.execute(prepared_query, (indexer,))
        __check_and_fix_orphan_entries(result, base_table, individual_index_table, orphan_primary_key_set,
                                       non_orphan_primary_key_set)
        logger.warning("Fixed table " + individual_index_table)


def __check_and_fix_orphan_entries(result, base_table, index_table, orphan_primary_key_set, non_orphan_primary_key_set):
    for row in result:
        where_values = []
        orphan_check_query: str
        primary_key_value: str = row[table_to_primary_key_mapping[base_table]]
        is_orphan = False
        if base_table in table_to_clustering_key_mapping.keys():
            clustering_key_value = row[table_to_clustering_key_mapping[base_table]]
            if (primary_key_value, clustering_key_value) in orphan_primary_key_set:
                is_orphan = True
            elif (primary_key_value, clustering_key_value) in non_orphan_primary_key_set:
                continue
        else:
            if primary_key_value in orphan_primary_key_set:
                is_orphan = True
            elif primary_key_value in non_orphan_primary_key_set:
                continue
        if is_orphan:
            logger.warning("Orphan Entry found from cache" + index_table + " " + str(row))
            if not read_mode:
                __delete_row(row, index_table)
            continue
        # Making is_orphan true by default, we change it's value to false if we find any entry in base table
        is_orphan = True
        if base_table in transposable_tables:
            where_values.append(primary_key_value)
            orphan_check_query = 'SELECT column_value from {} where partition_key=? limit 1'.format(base_table)
        else:
            where = table_to_primary_key_mapping[base_table] + '=?'
            where_values.append(row[table_to_primary_key_mapping[base_table]])
            if base_table in table_to_clustering_key_mapping.keys():
                where = '{} and {} =?'.format(where, table_to_clustering_key_mapping[base_table])
                where_values.append(row[table_to_clustering_key_mapping[base_table]])
            orphan_check_query = 'SELECT {} from {} where {} limit 1'.format(table_to_primary_key_mapping[base_table],
                                                                             base_table, where)
        orphan_prepared_query = __get_prepared_statement(orphan_check_query)
        result_from_base_table = session.execute(orphan_prepared_query, tuple(where_values))
        for entry in result_from_base_table:
            is_orphan = False
            logger.debug(entry)
        if is_orphan:
            if base_table in table_to_clustering_key_mapping.keys():
                orphan_primary_key_set.add((primary_key_value, row[table_to_clustering_key_mapping[base_table]]))
            else:
                orphan_primary_key_set.add(primary_key_value)
            logger.warning("Orphan Entry " + index_table + " " + str(row))
            if not read_mode:
                __delete_row(row, index_table)
        else:
            if base_table in table_to_clustering_key_mapping.keys():
                non_orphan_primary_key_set.add((primary_key_value, row[table_to_clustering_key_mapping[base_table]]))
            else:
                non_orphan_primary_key_set.add(primary_key_value)


"""
End of fix orphan entries block
"""

"""
Start of adding missing entry script
"""

def insert_missing_entry_for_tables_with_clustering_key(current_table_fields, base_table, reference_entry, index_table):
    if 'timestamp' in current_table_fields:
        current_table_fields.remove('timestamp')
    where = [table_to_primary_key_mapping[base_table], table_to_clustering_key_mapping[base_table]]
    query = get_query_for_selected_elements_from_table(current_table_fields, where, base_table)
    prepared_query = __get_prepared_statement(query)
    result = session.execute(prepared_query, (
        reference_entry[table_to_primary_key_mapping[base_table]],
        reference_entry[table_to_clustering_key_mapping[base_table]]))
    for row in result:
        insert_missing_entry(row, index_table)


def iterate_and_insert_missing_values(result, index_table_name):
    for row in result:
        insert_missing_entry(row, index_table_name)


def insert_missing_entry(missing_row, index_table_name):
    missing_row['timestamp'] = int(time.time())*1000000
    field_names = ','.join(missing_row.keys())
    positional_paramter = ['%s'] * len(missing_row.keys())
    field_values = ','.join(positional_paramter)
    query = 'insert into ' + index_table_name + ' (' + field_names + ') values (' + field_values + ')'
    session.execute(query, tuple(missing_row.values()))

def fix_Missing_entries(indexer, base_table):
    if base_table in transposable_tables:
        logger.log("Can't fix transposable tables")
        return
    for individual_index_table in table_to_index_table_mapping[base_table]:
        missing_entries = set()
        reference_table = table_to_index_table_mapping[base_table][0]
        if individual_index_table == reference_table:
            if len(table_to_index_table_mapping[base_table]) > 1:
                reference_table = table_to_index_table_mapping[base_table][1]
            else:
                logger.warning("No index table to compare. Exiting this table")
                return
        query = 'SELECT * FROM ' + individual_index_table + ' where ' + table_to_indexer_mapping[base_table] + '=?'
        prepared_query = __get_prepared_statement(query)
        current_table_result = session.execute(prepared_query, (indexer,))
        if not base_table in table_to_clustering_key_mapping.keys():
            table_to_clustering_key_mapping[base_table] = 'na'
        reference_table_query = 'SELECT * FROM ' + reference_table + ' where ' + table_to_indexer_mapping[
            base_table] + '=?'
        reference_table_prepared_query = __get_prepared_statement(reference_table_query)
        reference_table_result = session.execute(reference_table_prepared_query, (indexer,))
        current_primary_key_set = set()
        current_primary_key_to_clustering_key_dict = {}
        current_table_fields = set()
        for entry in current_table_result:
            if len(current_table_fields) == 0:
                current_table_fields = set(entry.keys())
            current_primary_key_set.add(entry[table_to_primary_key_mapping[base_table]])
            if not table_to_clustering_key_mapping[base_table] == 'na':
                if entry[table_to_primary_key_mapping[base_table]] in current_primary_key_to_clustering_key_dict.keys():
                    current_primary_key_to_clustering_key_dict[entry[table_to_primary_key_mapping[base_table]]].append(
                        entry[table_to_clustering_key_mapping[base_table]])
                else:
                    current_primary_key_to_clustering_key_dict[entry[table_to_primary_key_mapping[base_table]]] = [
                        entry[table_to_clustering_key_mapping[base_table]]]
        logger.warning(individual_index_table)
        for reference_entry in reference_table_result:
            if table_to_clustering_key_mapping[base_table] == 'na':
                if not reference_entry[table_to_primary_key_mapping[base_table]] in current_primary_key_set:
                    logger.warning("Missing Entry " + str(reference_entry))
                    missing_entries.add(reference_entry[table_to_primary_key_mapping[base_table]])
            else:
                if reference_entry[table_to_primary_key_mapping[base_table]] in current_primary_key_set:
                    if reference_entry[table_to_clustering_key_mapping[base_table]] not in \
                            current_primary_key_to_clustering_key_dict[
                                reference_entry[table_to_primary_key_mapping[base_table]]]:
                        logger.warning("Missing Entry " + str(reference_entry))
                        insert_missing_entry_for_tables_with_clustering_key(current_table_fields,base_table,reference_entry,individual_index_table)
                else:
                    logger.warning("Missing Entry " + str(reference_entry))
                    insert_missing_entry_for_tables_with_clustering_key(current_table_fields, base_table,
                                                                        reference_entry, individual_index_table)

        if table_to_clustering_key_mapping[base_table] == 'na':
            if len(missing_entries) == 0:
                continue
            if 'timestamp' in current_table_fields:
                current_table_fields.remove('timestamp')
            else:
                missing_entries = ['\'' + element + '\'' for element in missing_entries]
                in_clause = ','.join(missing_entries)
                select = ','.join(current_table_fields)
                base_table_query = 'SELECT ' + select + ' FROM ' + base_table + ' where ' + table_to_primary_key_mapping[
                    base_table] + ' in (' + in_clause + ')'
                result = session.execute(base_table_query)
                if not read_mode:
                    iterate_and_insert_missing_values(result, individual_index_table)



def run_for_a_base_table(option, base_table, indexer):
    if option == '1':
        __fix_duplicate_entries(indexer, base_table)
    if option == '2':
        __fix_orphan_entries(indexer, base_table)
    if option == '3':
        fix_Missing_entries(indexer, base_table)


def run_for_all_base_tables(option, indexer):
    for base_table in table_to_primary_key_mapping.keys():
        if base_table in table_to_indexer_mapping.keys():
            if table_to_indexer_mapping[base_table] == 'ent_id':
                run_for_a_base_table(option, base_table, indexer)


def start_script_execution():
    if indexer_range == 'one':
        indexer = input('Enter Indexer value \n')
        if table == 'all':
            run_for_all_base_tables(operation, indexer)
        else:
            run_for_a_base_table(operation, table, indexer)
    elif indexer_range == 'selected':
        indexer_file = input('Enter File to read for indexer (ents - comma separated): \n')
        ents_text_file = open(indexer_file, "r")
        ents = ents_text_file.read().split(',')
        ents = [ent.strip() for ent in ents]
        print(ents)
        for ent in ents:
            if table == 'all':
                run_for_all_base_tables(operation, ent)
            else:
                print('dd')
                # run_for_a_base_table(operation, table, ent)
    elif indexer_range == 'all':
        wf_query = 'SELECT distinct partition_key FROM wf_ents'
        ent_details = __get_prepared_statement(wf_query)
        WfentsResult = session.execute(ent_details)
        for ent in WfentsResult:
            if table == 'all':
                run_for_all_base_tables(operation, ent['partition_key'])
            else:
                run_for_a_base_table(operation, table, ent['partition_key'])
    else:
        logger.critical('Wrong entry for indexer range ')
        print('wrong entry for indexer. Value can be one ,all or selected')


start_script_execution()
query_file.close()
